from Adafruit_I2C import Adafruit_I2C
import Adafruit_BBIO.PWM as PWM
from time import sleep
import time
# initialize i2c connection to MPU6050
# i2c address is 0x68
i2c = Adafruit_I2C(0x68)
# wake up the device (out of sleep mode)
# bit 6 on register 0x6B set to 0
i2c.write8(0x6B, 0)
print("X axis accelerations (in g's)")
# read and print acceleration on x axis
# Most significant byte on 0x3b
# Least significant byte on 0x3c
# Combined to obtain raw acceleration data
PWM.start("P9_14",7,50 )#motor 1 

while True:      
        b = i2c.readS8(0x3b)   # getting values from the registers  
        s = i2c.readU8(0x3c)   # converting 2 8 bit words into a 16 bit
        raw = b * 256 + s        # signed "raw" value       
        g = raw / 16384. # still needs to be converted into G-forces
        print (str(g))
        sleep(0.2)
        if g >0.7:
            PWM.set_duty_cycle("P9_14", 6.4)
        elif g<0.72:
                PWM.set_duty_cycle("P9_14", 7.6)