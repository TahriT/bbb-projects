from Adafruit_I2C import Adafruit_I2C
from time import sleep
 
#Thanks http://andrewdai.co/beaglecar/sensors-and-interfaces.html
#for the basics
#
#Thanks https://github.com/cTn-dev/PyComms/blob/master/MPU6050/mpu6050.py
#for the addresses
#
# initialize i2c connection to MPU6050
# i2c address is 0x68
i2c = Adafruit_I2C(0x68)

# wake up the device (out of sleep mode)
# bit 6 on register 0x6B set to 0
i2c.write8(0x6B, 0)

while True:
        x = 1

        accelX_list = [0.0, 0.0, 0.0, 0.0, 0.0]

        accelY_list = [0.0, 0.0, 0.0, 0.0, 0.0]

        accelZ_list = [0.0, 0.0, 0.0, 0.0, 0.0]

        gyroX_list = [0.0, 0.0, 0.0, 0.0, 0.0]

        gyroY_list = [0.0, 0.0, 0.0, 0.0, 0.0]

        gyroZ_list = [0.0, 0.0, 0.0, 0.0, 0.0]

        for x in range(0, 5):
                accelX_H = i2c.readS8(0x3b)
                accelX_L = i2c.readU8(0x3c)

                accelY_H = i2c.readS8(0x3d)
                accelY_L = i2c.readU8(0x3e)

                accelZ_H = i2c.readS8(0x3f)
                accelZ_L = i2c.readU8(0x40)

                gyroX_H = i2c.readS8(0x43)
                gyroX_L = i2c.readU8(0x44)

                gyroY_H = i2c.readS8(0x45)
                gyroY_L = i2c.readU8(0x46)

                gyroZ_H = i2c.readS8(0x47)
                gyroZ_L = i2c.readU8(0x48)                

                accelX_raw = accelX_H * 256.0 + accelX_L

                accelY_raw = accelY_H * 256.0 + accelY_L

                accelZ_raw = accelZ_H * 256.0 + accelZ_L

                #Values for reading gyros
                fullRangeReading = 32767 + 32768 #sensor output readings
                fullRotationReading = 500.0 #degrees/second
                calVal = fullRotationReading / fullRangeReading

                gyroX_raw = gyroX_H * calVal + gyroX_L

                gyroY_raw = gyroY_H * calVal + gyroY_L

                gyroZ_raw = gyroZ_H * calVal + gyroZ_L

                accelX_list[x] = accelX_raw

                accelY_list[x] = accelY_raw

                accelZ_list[x] = accelZ_raw

                gyroX_list[x] = gyroX_raw

                gyroY_list[x] = gyroY_raw

                gyroZ_list[x] = gyroZ_raw

        accelX_avg = sum(accelX_list) / len(accelX_list)

        accelY_avg = sum(accelY_list) / len(accelY_list)

        accelZ_avg = sum(accelZ_list) / len(accelZ_list)

        gyroX_avg = sum(gyroX_list) / len(gyroX_list)

        gyroY_avg = sum(gyroY_list) / len(gyroY_list)

        gyroZ_avg = sum(gyroZ_list) / len(gyroZ_list)

        print(str(accelX_avg), "\t", str(accelY_avg), "\t", str(accelZ_avg), "\t", str(gyroX_avg), "\t", str(gyroY_avg), "\t", str(gyroZ_avg))
        sleep(0.3)