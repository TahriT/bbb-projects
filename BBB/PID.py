#Dis be code Written by Da Catt
#
#"Simple" PID controller for senior design
#Tahri "Da Boss" Turner probably made some corrections
#
#links:
#https://wiki.python.org/moin/WhileLoop
#http://www.pythonforbeginners.com/basics/python-if-elif-else-statement
#https://docs.python.org/release/1.5.1p1/tut/functions.html
#https://docs.python.org/2/reference/simple_stmts.html#the-print-statement
#
#Let's get to it shall we?

import math
from Adafruit_I2C import Adafruit_I2C
import Adafruit_BBIO.PWM as PWM
import time
from hopesForSensors import sensorOutputs

#DeTestable changes
#These may be different depending on motor and esc combo
#Make em pretty aight?
global maxMotorSpeedLeft 
maxMotorSpeedLeft = 8.0
global minMotorSpeedLeft
minMotorSpeedLeft = 7.6
global maxMotorSpeedRight
maxMotorSpeedRight = 5.8
global minMotorSpeedRight
minMotorSpeedRight = 6.2

#The angle we want to correct for. In degrees, cause we racy like that
global correctionAngle
correctionAngle = 5
global desiredGyroX
desiredGyroX = 0

#PID controllery bits. You know you like em
global integralK
integralK = 1

#Gravity, whats that?
global gravity
gravity = 9.806

#Distant from da pivot?
global pivotDistance
pivotDistance = 0.2237

def runleft():
    #This is to correct for a tilt to the right
    PWM.start("P9_14",7,50)
    #Gyrating the body ;)
    gyroX = sensorOutputs[4]
    #gyroY = sensorOutputs[5]
    #Finding the differences
    differenceGyroX = abs(gyroX - desiredGyroX)
    #differenceGyroY = abs(gyroY - desiredGyroY)
    #
    #Gyro outputs between 0 and 250(?) for right tilts
    #WHAT DOES IT MEAN?!
    gyroToDegrees = 250/90
    gyroDegreesX = gyroToDegrees * differenceGyroX
	#gyroDegreesY = gyroToDegrees * differenceGyroY
	#
	#So uh... How fast you want to go?
	#Find out with Y = mX + B brotha man!
	#Y is desired speed. m is current degree of tilt.
	#X is angle to get motor stuff. B is correction factor thing
    proportionalMotorSpeed = (gyroDegreesX / correctionAngle) * (maxMotorSpeedLeft - minMotorSpeedLeft) + minMotorSpeedLeft
    #
	#Come here often?
    leftLoop = 0
    rightLoop += 1
	#
	#Sounds like you may need a "speed" adjustment ;)
    integralMotorSpeed = rightLoop * integralK * proportionalMotorSpeed
	#
	#Where you think your going?
    accelerometerGeesX = sensorOutputs[1]
	#
	#This can be used to find the D in the PID controller
    PWM.set_duty_cycle("P9_14", proportionalMotorSpeed)

def runright():
    PWM.start("P9_14",7,50 )
	#Run left but in other direction.
	#Gyrating the body ;)
    gyroX = sensorOutputs[4] 
    #gyroY = sensorOutputs[5]
    desiredGyroX, desiredGyroY = 0.0, 0.0
	#
    differenceGyroX = abs(gyroX - desiredGyroX)
    #differenceGyroY = abs(gyroY - desiredGyroY)
	#
	#Gyro outputs between 0 and 250(?) for right tilts
	#WHAT DOES IT MEAN?!
    gyroToDegrees = 250/90
    gyroDegreesX = gyroToDegrees * differenceGyroX
    gyroDegreesY = gyroToDegrees * differenceGyroY
	#
	#So uh... How fast you want to go?
	#Find out with Y = mX + B brotha man!
	#Y is desired speed. m is current degree of tilt.
	#X is angle to get motor stuff. B is correction factor thing
    proportionalMotorSpeed = (gyroDegreesX / correctionAngle) * (maxMotorSpeedLeft - minMotorSpeedLeft) + minMotorSpeedLeft
	#
	#Come here often?
    leftLoop = 0
    rightLoop += 1
	#
	#Sounds like you may need a "speed" adjustment ;)
    integralMotorSpeed = rightLoop * integralK * proportionalMotorSpeed
	#
	#Where you think your going?
    accelerometerGeesX, accelerometerGeesY = sensorOutputs[1, 2]
	#
	#
	
	#This can be used to find the D in the PID controller
    PWM.set_duty_cycle("P9_14", proportionalMotorSpeed)
    
    
#def jump(): #Jump and time delay to the jump
	#run motor at high speed then stop

def balance():	#Balances within the specified n.
	#print "Balancing between 0 and , n"
    while True:
        gyroDegreesX  = sensorOutputs(4)
        gyroDegreesY = sensorOutputs(5)
        print("Gyro X:", gyroDegreesX, "Gyro Y:", gyroDegreesY)
        if abs(gyroDegreesX) > correctionAngle or abs(gyroDegreesY) > correctionAngle:
            PWM.stop ("P9_14")
            PWM.cleanup()
            break
        elif gryoDegreesX > 0:
            runleft() #Calls the balance left to set motor speed
        elif gyroDegreesX < 0:
            runright()	#Calls the balance right to set motor speed.
        else:
            print ("WOO!")

class main:
    while True:
	    n = raw_input("Would you like me to balance?")

	    if n.strip() == 'yes':
		    run = True
	    else:
		    print "Okay... :("
		    break	#Think this exits the program.

	    while run:
		    #jump()		#Tells cube to jump
		    balance()	#Tells to balance and the desired balance range.
		    break	#Think this ends after balance runs.
