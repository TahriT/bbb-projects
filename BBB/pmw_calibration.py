import Adafruit_BBIO.PWM as PWM
import time

PWM.start("P9_14",7,50 )

while True:
    i = input("Enter a number between 4 and 10, otherwise hit 0\n")
    if i>=0 and i <=10: # 4 Full Reverse, 7 Stop, 10 Full Foward
    	PWM.set_duty_cycle("P9_14", float(i))
    	print "Duty set to: ",i,""
    else:
    	break

print 'User has exited, shutting down Pin and cleaning up.'
PWM.stop ("P9_14")
PWM.cleanup()
quit()